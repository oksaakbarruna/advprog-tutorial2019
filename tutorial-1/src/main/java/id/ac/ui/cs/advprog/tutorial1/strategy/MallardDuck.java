package id.ac.ui.cs.advprog.tutorial1.strategy;

public class MallardDuck extends Duck {
    // TODO Complete me!
    public MallardDuck() {
    	flyBehavior = new FlyWithWings();
    	quackBehavior = new Quack();
    }

    public void display() {
    	System.out.println("I am a real Mallard duck");
    }
}
